from kivymd.app import MDApp
from kivymd.uix.navigationdrawer import MDNavigationDrawer

from context import app_context

origin_func = MDNavigationDrawer.on_kv_post

def clear_radius():
    _context = app_context.get()
    app = _context['main_app']
    image_emulator = app.root.ids.main_screen_emulator.ids.image_emulator
    if image_emulator.source == './assets_emulator/image_emulator.png':
        return True
    return False

def on_kv_post(self, base_widget):
    if not getattr(self, '_drawer_emulator', False):
        self.size_hint_x = .8
        if clear_radius():
            self.radius = 0
    return origin_func(self, base_widget)

MDNavigationDrawer.on_kv_post = on_kv_post