from kivy.lang import Builder
from kivy.properties import ObjectProperty
from kivy.utils import platform
from kivy.clock import mainthread

from kivymd.app import MDApp
from kivymd.uix.boxlayout import MDBoxLayout
from kivymd.uix.filemanager import MDFileManager
from kivymd.uix.navigationdrawer import MDNavigationDrawerMenu

from screens_emulator.main_screen import WrapperScreenEmulator
from monitor import FolderMonitor

from watchdog.observers import Observer
import importlib, logging, traceback
import os, sys, time
from contextvars import copy_context
from functools import partial

logger = logging.getLogger(__name__)

Builder.load_string(
'''

<BoxDrives>:

<ContentNavigationDrawerMenu>:
    orientation: 'vertical'
    choose_file: input_choose_file
    class_name: input_class_name
    spacing: '5dp'
    MDNavigationDrawerHeader:
        text: 'Configuration'
        source: 'assets_emulator/icon-conf.png'
    MDNavigationDrawerDivider:
    MDBoxLayout:
        orientation: 'vertical'
        adaptive_height: True
        spacing: 10
        BoxDrives:
            id: box_drives
            orientation: 'horizontal'
            adaptive_size: True
            size_hint_x: 1

    MDRaisedButton:
        text:'Choose file'
        size_hint: 1, None
        on_release: root.file_manager_open()
    MDTextFieldRect:
        id: input_choose_file
        size_hint: 1, None
        height: dp(30)
    MDTextFieldRect:
        id: input_class_name
        size_hint: 1, None
        height: dp(30)
        hint_text: 'Name class'
    MDRaisedButton:
        text:'Load'
        size_hint: 1, None
        on_release: root.rebuild(from_menu=True)

'''
)


class BoxDrives(MDBoxLayout):
    def removes_marks_all_chips(self, instance):
        for children in self.children:
            if not children == instance:
                children.active = False


class ContentNavigationDrawerMenu(MDNavigationDrawerMenu):
    choose_file = ObjectProperty()
    class_name = ObjectProperty()
    filemanager = None
    last_error = None
    last_error_time = 0

    def file_manager_open(self):
        self.file_manager = MDFileManager(
            exit_manager=self.exit_manager,
            select_path=self.select_path)

        chip, = [children for children in self.ids.box_drives.children
            if children.active]

        drive = '%s\\' % chip.text if platform == 'win' else chip.text
        self.file_manager.show(drive)

    def select_path(self, path):
        self.choose_file.text = path
        self.check_handler(os.path.dirname(path))
        self.exit_manager()

    def exit_manager(self, *args):
        self.file_manager.close()

    def check_handler(self, path):
        app = MDApp.get_running_app()
        app.observer_handler = Observer()
        app.observer_handler.schedule(FolderMonitor(app), path)
        app.observer_handler.start()

    def show_emulate_error(self):
        error = traceback.format_exc()
        endtime = time.time()
        if not self.last_error == error or (endtime - self.last_error_time) > 1:
            sep = '=' * 45
            msg_error = 'Start logging error: %s\n\n %s \n\n' \
                'End logging error: %s\n\n' % (sep, error, sep)
            logger.error(msg_error)
            self.last_error = error
            self.last_error_time = endtime

    @mainthread
    def rebuild(self, app=None, from_menu=False):
        filename = os.path.basename(self.choose_file.text)
        dirname = os.path.dirname(self.choose_file.text)
        _class = self.class_name.text

        sys.path.insert(0, dirname)
        module = filename.split('.')[0]

        try:
            _file = importlib.import_module(module)
            importlib.reload(_file)
        except Exception as _error:
            _file = None
            self.show_emulate_error()

        app = MDApp.get_running_app() if not app else app
        _class = getattr(_file, _class) if _file and _class else None

        if _file and _class:
            box = app.root.ids.main_screen_emulator.ids.box_emulator
            box.clear_widgets()

            screen = WrapperScreenEmulator(kivymd_class=_class)
            try:
                _context = copy_context()
                build = screen.build()
                func = partial(box.add_widget, build)
                _context.run(func)
            except Exception as _error:
                self.show_emulate_error()

        if from_menu:
            app.root.ids.navigation_drawer.set_state()
